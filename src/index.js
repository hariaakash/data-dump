const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const shortid = require('shortid');

const db = require('./lowdb');

const port = process.env.PORT || 3000;

const app = express();

app.use(cors());
app.use(bodyParser.json());

app.post('/', (req, res) => {
	const id = shortid.generate();
	db.get('dump').push({ id, ...req.body }).write();
	res.json({ status: true, data: { id } });
});

app.get('/:id', (req, res) => {
	let data;

	if (req.params.id && req.params.id === 'list') data = db.get('dump').value();
	else if (req.params.id) data = db.get('dump').find({ id: req.params.id }).value();

	res.json({ status: true, data: data || {} });
});

app.listen(port);

console.log('Server running on ' + port)